from django import forms
from .models import Feedback

class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = [
            'name',
            'feedback',
        ]

        widgets = {
            'name' : forms.TextInput(attrs={'class' : 'form-control add-form', 'id' : 'name_field', 'placeholder' : 'Masukkan nama anda di sini ...'}),
            'feedback' : forms.Textarea(attrs={'class' : 'form-control add-form', 'id' : 'feedback_field', 'placeholder' : 'Masukkan feedback di sini ...'}),
        }