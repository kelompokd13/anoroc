from django.test import TestCase, LiveServerTestCase, TestCase, tag
from django.urls import reverse
from selenium import webdriver
from django.urls import resolve
from .views import feedbackView
from .models import Feedback
from django.utils import timezone
from .forms import FeedbackForm

class FeedbackTest (TestCase):
    def test_url_exists (self):
        response = self.client.get('/feedback/')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_func (self):
        comp = resolve('/feedback/')
        self.assertEqual(comp.func, feedbackView)

    def test_write_feedback (self):
        obj = Feedback.objects.create(
            name = 'Pak Pewe',
            feedback = 'Nice',
            timestamp = timezone.now()
        )
        self.assertTrue(isinstance(obj, Feedback))
        self.assertEqual(obj.__str__(), obj.feedback)

    def test_valid_form (self):
        obj = Feedback.objects.create(
            name = 'Pak Pewe',
            feedback = 'Nice',
            timestamp = timezone.now()
        )
        data = {
            'feedback' : obj.feedback,
            'name' : obj.name,
            'timestamp' : obj.timestamp
        }
        form = FeedbackForm(data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_form (self):
        obj = Feedback.objects.create(
            name = '',
            feedback = '',
            timestamp = timezone.now()
        )
        data = {
            'feedback' : obj.feedback,
            'timestamp' : obj.timestamp
        }
        form = FeedbackForm(data=data)
        self.assertFalse(form.is_valid())
