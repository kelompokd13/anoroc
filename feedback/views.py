from django.shortcuts import render, redirect
from .models import Feedback
from .forms import FeedbackForm

# Create your views here.

def feedbackView(request):
    feedback = Feedback.objects.all()
    feedbackForm = FeedbackForm(request.POST or None)
    if request.method == "POST":
        if feedbackForm.is_valid():
            feedbackForm.save()
            return render(request, 'feedback/feedback.html', {'feedback' : feedback, 'form' : feedbackForm})

    context = {
        'feedback' : feedback,
        'form' : feedbackForm,
    }

    return render(request, 'feedback/feedback.html', context)
