from django.db import models
from django.utils import timezone


# Create your models here.

class Feedback(models.Model):
    name = models.CharField(max_length = 50)
    feedback = models.TextField(max_length=500)
    timestamp = models.DateTimeField(default=timezone.now)

    objects = models.Manager()

    def __str__(self):
        return self.feedback