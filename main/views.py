from django.shortcuts import render


def home(request):
    return render(request, 'main/home.html')

def comment(request):
    return render(request, 'comment.html')