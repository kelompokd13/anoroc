from django.shortcuts import render, redirect
from .models import Article, Comment
from .forms import ArticleForm, CommentForm
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
import json

from django.core import serializers
from django.http import HttpResponse

def article_view(request):
    articles = Article.objects.all().order_by('judul')
    return render(request, 'articles/article_view.html', {'articles':articles})

@login_required
def article_create(request):
    form = ArticleForm()
    if request.method == 'POST':
        form = ArticleForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/article')

    return render(request, 'articles/article_form.html', {'form':form})

def article_detail(request,slug):
    article = Article.objects.get(slug=slug)
    comments = Comment.objects.all().order_by('date_added')
    form = CommentForm()

    if request.method == 'POST':
        form = CommentForm(request.POST)
        judulnya =  request.user.username
        isinya = request.POST.get('isi')
        Comment.objects.create(judul = judulnya, isi = isinya, article = article)
        # json_data.get('comment').append(response_data)
        # js_data = json.dumps(json_data)
        # return JsonResponse(json_data)
        return render(request, 'articles/article_detail.html', {'article':article, 'form':form, 'comments':comments})

    # return JsonResponse(json_data)
    return render(request, 'articles/article_detail.html', {'article':article, 'form':form, 'comments':comments})

def json_comment(request,slug):
    article = Article.objects.get(slug=slug)
    json_data = {}
    json_data['comment'] = []
    for obj in Comment.objects.all() :
        if obj.article == article :
            response_data = {}
            response_data['author'] = obj.judul
            response_data['isi'] = obj.isi
            json_data.get('comment').append(response_data)

    return JsonResponse(json_data)

# Mengubah Model menjadi Json.
def article_json(request):
    articles = Article.objects.all().order_by('judul')
    articles_list = serializers.serialize('json',articles)
    return HttpResponse(articles_list, content_type="text/json-comment-filtered")

# Menampilkan Article dengan Ajax, Json.
def article_view_json(request):
    return render(request, 'articles/article_view_json.html')