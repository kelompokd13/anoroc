import string
import random

from django.db import models
from django.utils.text import slugify

def rand_slug():
    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(6))

class Article(models.Model):
    judul = models.CharField(max_length=100)
    isi = models.TextField()
    image = models.ImageField(default='default.png', blank=True)
    slug = models.SlugField(max_length=100, unique=True)

    def snippet(self):
        return self.isi[:100] + '...'

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(rand_slug() + "-" + self.judul)
        super(Article, self).save(*args, **kwargs)

class Comment(models.Model):
    judul = models.CharField(max_length=100)
    isi = models.CharField(max_length=500)
    date_added = models.DateTimeField(auto_now_add=True)

    article = models.ForeignKey(Article, on_delete = models.CASCADE, null=True)