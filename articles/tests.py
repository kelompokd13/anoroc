from django.test import TestCase, LiveServerTestCase, TestCase, tag
from django.urls import reverse
from selenium import webdriver
from django.urls import resolve
from .views import article_create, article_detail, article_view
from .models import Article, Comment
from django.utils import timezone
from .forms import ArticleForm, CommentForm
from django.test import Client

class article(TestCase):
    def test_url_exist(self):
        response = self.client.get('/article/')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_func(self):
        comp = resolve('/article/')
        self.assertEqual(comp.func, article_view)

    def test_landing_page_template(self):
        response = self.client.get('/article/')
        self.assertTemplateUsed(response, 'articles/article_view.html')
        
    def test_create_page_without_login(self):
        response = self.client.get('/article/article_create')
        self.assertRedirects(response, '/auth/?next=/article/article_create', status_code=302, target_status_code=200, msg_prefix='', fetch_redirect_response=True)
        
class ModelTest(TestCase):
    def setUp(self):
        self.article = Article.objects.create(
            judul="Ini Artikel", isi="Recovery rate corona sekarang sudah 100%")
        self.comment = Comment.objects.create(judul="Meliana", isi="Keren!")

    def test_instance_created(self):
        self.assertEqual(Article.objects.count(), 1)
        self.assertEqual(Comment.objects.count(), 1)

    # def test_str(self):
    #     self.assertEqual(str(self.article), "Ini Artikel")
    #     self.assertEqual(str(self.comment), "Meliana")
    
class FormTest(TestCase):
    def test_valid_comment_form(self):
        obj = Comment.objects.create(
            judul = 'Meliana',
            isi = 'Artikelnya bagus!',
            date_added = timezone.now()
        )
        data = {
            'judul' : obj.judul,
            'isi' : obj.isi,
            'date_added' : obj.date_added
        }
        form = CommentForm(data=data)
        self.assertTrue(form.is_valid())
        
    def test_if_comment_form_is_invalid(self):
        obj = Comment.objects.create(
            judul = '',
            isi = '',
            date_added = timezone.now()
        )
        data = {
            'judul' : obj.judul,
            'isi' : obj.isi,
            'date_added' : obj.date_added
        }
        form = CommentForm(data=data)
        self.assertFalse(form.is_valid())
        
class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.article_view = reverse("article_view")
        self.article_create = reverse("article_create")
        # self.article_detail = reverse("article_detail")

    def test_GET_article_view(self):
        response = self.client.get(self.article_view)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'articles/article_view.html')

    def test_GET_article_create(self):
        response = self.client.get(self.article_create)
        self.assertEqual(response.status_code, 302)

    def test_POST_addArticle(self):
        response = self.client.post('/article/article_create',
            {
                'judul': 'Ini Artikel',
                'isi': "Recovery rate corona sekarang sudah 100%"
            }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_addComment(self):
        self.article = Article.objects.create(judul="Ini Artikel", isi="Recovery rate corona sekarang sudah 110%", slug="articleSlug")

        articleUrl = '/article/' + self.article.slug

        response = self.client.post(articleUrl,
            {
                'judul': 'Ini Comment',
                'isi': "Recovery rate corona sekarang sudah 100% belum ya?"
            }, follow=True)

        self.assertEqual(response.status_code, 200)

        new_response = self.client.post(articleUrl)
        html_response = new_response.content.decode('utf8')
        self.assertIn("", html_response)
