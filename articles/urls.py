from django.urls import path
from . import views

#app_name = 'article'

urlpatterns = [
    path('', views.article_view, name='article_view'),
    path('article_create', views.article_create, name='article_create'),
    path('<slug:slug>/', views.article_detail, name='detail'),
    path('<slug:slug>/jsondata', views.json_comment, name='json_comment'),

    path('data/json/', views.article_json, name='jsondata'),
    path('json/view/', views.article_view_json, name="article_view_json"),

    #FUTURE UPDATES
    #path('article_update/<str:pk>/', views.article_update, name='article_update'),
    #path('article_delete/<str:pk>/', views.matkul_delete, name='matkul_delete'),
    
]