from django.forms import ModelForm
from django import forms
from .models import Article, Comment

class ArticleForm(ModelForm):
    class Meta:
        model = Article
        fields = ('judul', 'isi', 'image')
        
        widgets = {
            'judul': forms.TextInput(attrs={
                'class': 'form-control add-form',
                'placeholder': 'Judul artikel',
            }),
            'isi': forms.Textarea(attrs={
                'class': 'form-control add-form',
                'placeholder': 'Isi artikel',
            }),
        }

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ('isi',)
        
        widgets = {
            'isi': forms.Textarea(attrs={
                'class': 'post-text form-control',
                'placeholder': 'Tulis komentarmu di sini..',
            }),
        }