from django.urls import path
from . import views

app_name = 'statistic'
urlpatterns = [
    path('', views.statistic_view, name = 'statistic_view'),
    path('data/', views.statistic_data, name = 'statistic_data'),
    path('data/provinsi/', views.province_data, name = 'province_data'),
]
