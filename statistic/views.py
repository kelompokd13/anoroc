from django.shortcuts import render
from django.http import JsonResponse
from pip._vendor import requests
import json

def statistic_view(request):
    return render(request, 'statistics/statistic_view.html')

def statistic_data(request):
    url = "https://api.kawalcorona.com/indonesia/"
    ret = requests.get(url)
    data = json.loads(ret.content)
    return JsonResponse(data, safe=False)
    
def province_data(request):
    url = "https://api.kawalcorona.com/indonesia/provinsi"
    ret = requests.get(url)
    data = json.loads(ret.content)
    print(data)
    return JsonResponse(data, safe=False)

