// Menampilkan statistik covid Indonesia dari api kawalcorona
$(function(item){

    $.ajax({

        url: 'data/',
        success: function(data) {
            $("#list_statistic").empty();

            positif = data[0].positif;
            sembuh = data[0].sembuh;
            meninggal = data[0].meninggal;

            $("#list_statistic").append(
                "<li>" + "Kasus positif:   " + positif + "</li>" +
                "<li>" + "Yang telah sembuh:   " + sembuh + "</li>" +
                "<li>" + "Jumlah meninggal:  " + meninggal + "</li>"
            )
        }

    });
});

// Menampilkan statistik covid di Provinsi Indonesia dengan mengclick
$(".buttonProvinsi").click(function(item){
    var provinsi = $("#keywordProvinsi").val();

    $.ajax({

        url: 'data/provinsi/',
        success: function(data) {

            for (a = 0; a < data.length; a++) {

            var data1 = data[a].attributes.Provinsi;
            var data2 = provinsi;
            var comperator = data1.localeCompare(data2);

            if ( comperator == 0 ) {

                provinsi = data[a].attributes.Provinsi;
                positif = data[a].attributes.Kasus_Posi;
                sembuh = data[a].attributes.Kasus_Semb;
                meninggal = data[a].attributes.Kasus_Meni;

                $("#list_province").append(
                    "<li>" + "Provinsi: " + provinsi + "</li>" +
                    "<li>" + "Kasus positif: " + positif + "</li>" +
                    "<li>" + "Yang telah sembuh: " + sembuh + "</li>" +
                    "<li>" + "Jumlah meninggal: " + meninggal + "</li>" +
                    "<li>" + "</li>"
                )
            }
            }
        }
    });
});

// Menampilkan statistik covid di Provinsi Indonesia dengan key Enter
$("#keywordProvinsi").keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        
        var provinsi = $("#keywordProvinsi").val();

    $.ajax({

        url: 'data/provinsi/',
        success: function(data) {

            for (a = 0; a < data.length; a++) {

            var data1 = data[a].attributes.Provinsi;
            var data2 = provinsi;
            var comperator = data1.localeCompare(data2);

            if ( comperator == 0 ) {

                provinsi = data[a].attributes.Provinsi;
                positif = data[a].attributes.Kasus_Posi;
                sembuh = data[a].attributes.Kasus_Semb;
                meninggal = data[a].attributes.Kasus_Meni;

                $("#list_province").append(
                    "<li>" + "Provinsi: " + provinsi + "</li>" +
                    "<li>" + "Kasus positif: " + positif + "</li>" +
                    "<li>" + "Yang telah sembuh: " + sembuh + "</li>" +
                    "<li>" + "Jumlah meninggal: " + meninggal + "</li>" +
                    "<li>" + "</li>"
                )
            }
            }
        }
    });
    }
});

// Menghapus statistik covid di Provinsi Indonesia
$(".buttonProvinsiClear").click(function(item){
    $("#list_province").empty();
});

// Article page
// -------------------------------------------------------------
$(document).ready(function () {

    $.ajax({

        url: '/article/data/json/',
        success: function(data) {
            console.log(data)
            for ( a = 0; a < data.length; a++) {
                judul = data.judul;
                slug = data.slug;
                image = data.image;
                snippet = data.snippet;

                $("#semuaArtikel").append(
                    ' <div class="divCard"> ' +
                        ' <a href= ' + ' " ' + '{% url "detail" ' + ' slug= ' + data[a].fields.slug + ' %}"> ' +
                        ' <div class="card-container"> ' +
                            ' <div class="card article-card"> ' +
                                ' <div class="row"> ' +
                                    ' <div class="col-lg-3" style="text-align:center;"> ' +
                                        ' <img src=" ' + ' ' + data[a].fields.image + ' " class="article-image"/> ' +
                                    ' </div> ' +
                                    ' <div class="col-lg-9"> ' +
                                        ' <br> ' +
                                        ' </br><h2 class="article-judul judulArticleJson"> ' + ' ' + data[a].fields.judul + '</h2> ' +
                                        ' <h2 class="article-isi"> ' + data[a].fields.isi + ' </h2> ' +
                                    ' </div> ' +
                                ' </div> ' +
                            ' </div> ' +
                        ' </div> ' +
                        ' </a> ' +
                    ' </div> '
                //   {% comment %} <h3><a href="{% url 'detail' slug=article.slug %}">{{ article.judul }}</a></h3>
                //   <p>{{ article.snippet }}</p>
                //   <br> {% endcomment %}
                )
            }
            
        }

    });
});