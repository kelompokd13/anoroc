$(document).ready(function() {

    $('#reload').click(function() {
        var url_data = "/data"
        $.ajax({
            url: url_data,
            success: function(data) {
                var obj_hasil = $('#search');
                obj_hasil.empty()

                for (i=0; i < data.length; i++) {
                    var message = data[i].message
                    obj_hasil.append('<div class="col-sm-3 pesan">'+ message +'</div>');
                }
            }
        });
    });
});