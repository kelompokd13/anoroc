from django.urls import path
from .views import loginUser, registerUser, logoutUser

app_name = 'login'
urlpatterns = [
    path('', loginUser, name = 'loginUser'),
    path('register/', registerUser),
    path('logout/', logoutUser),
]
