from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from .forms import Form_aja

# Create your views here.

def loginUser(request):
    if request.method == "POST":
        form = Form_aja(request.POST)
        if (form.is_valid()):
            userName = form.cleaned_data["user_name"]
            passWord = form.cleaned_data["Password"]
            user = authenticate(request, username = userName, password = passWord)
            if user is not None:
                login(request, user)
                return redirect("/")
        return redirect("/auth/")
    else:
        
        form = Form_aja()
        response = {
            "form" : form
        }
        return render(request, "login/login.html", response)

def registerUser(request):
    if request.method == "POST":
        form = Form_aja(request.POST)
        if (form.is_valid()):
            userName = form.cleaned_data["user_name"]
            passWord = form.cleaned_data["Password"]
            try:
                user1 = User.objects.get(username = userName)
                return redirect("/auth/register/")
            except User.DoesNotExist:
                user = User.objects.create_user(userName, None, passWord)
                return redirect("/auth/")
    else:
        form = Form_aja()
        response = {
            "form" : form
        }
        return render(request, "login/register.html", response)

def logoutUser(request):
    logout(request)
    return redirect("/")
