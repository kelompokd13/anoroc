from django import forms

class Form_aja(forms.Form):
    user_name = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'User Name',
        'type' : 'text',
        'required' : True,
        'help_text' : True
    }))
    Password = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Password',
        'type' : 'password',
        'required' : True,
        'help_text' : True
    }))
