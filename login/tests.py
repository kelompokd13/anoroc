from django.test import TestCase, LiveServerTestCase
from django.http import HttpRequest
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time

from .views import loginUser, registerUser, logoutUser

# Create your tests here.
class UnitTest(TestCase):

    def test_url_does_exist(self):
        response = self.client.get('/auth/')
        self.assertEqual(response.status_code, 200)
        response2 = self.client.get('/auth/logout/')
        self.assertEqual(response2.status_code, 302)
        response3 = self.client.get('/auth/register/')
        self.assertEqual(response3.status_code, 200)
        
    def test_url_fires_correct_method(self):
        response = resolve('/auth/')
        self.assertEqual(response.func, loginUser)
        response2 = resolve('/auth/logout/')
        self.assertEqual(response2.func, logoutUser)
        response3 = resolve('/auth/register/')
        self.assertEqual(response3.func, registerUser)
    
    def test_using_correct_template(self):
        response = self.client.get('/auth/')
        self.assertTemplateUsed(response, 'login/login.html')
        response3 = self.client.get('/auth/register/')
        self.assertTemplateUsed(response3, 'login/register.html')
    
    def test_text_inside_html(self):
        request = HttpRequest()
        response = loginUser(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Login",html_response)
    
    def test_trying_login(self):
        response = self.client.post('/auth/', data={'user_name' : 'coba', 'Password' : 'coba'})
        html_response = response.content.decode()
        self.assertIn(html_response, 'Hi, coba')
    
    def test_trying_register_with_existed_account(self):
        response = self.client.post('/auth/register/', data={'user_name' : 'coba', 'Password' : 'coba'})
        html_response = response.content.decode()
        self.assertIn(html_response, 'Register')