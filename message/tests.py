from django.test import TestCase, LiveServerTestCase, TestCase, tag
from django.urls import reverse
from selenium import webdriver
from django.urls import resolve
from .views import view
from .models import Message
from django.utils import timezone
from .forms import MessageForm

# Create your tests here.
class message (TestCase):
    def test_url_exist (self):
        response = self.client.get('/messagetext/')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_func(self):
        comp = resolve('/messagetext/')
        self.assertEqual(comp.func, view)

    def test_landing_page_template(self):
        response = self.client.get('/messagetext/')
        self.assertTemplateUsed(response, 'message/messagetext.html')

    def test_model_create_message(self):
        Message.objects.create(
            message = 'Semangat pejuang corona',
            time = timezone.now()
        )

        count_all_message = Message.objects.all().count()
        self.assertEqual(count_all_message, 1)

    def test_model_func(self):
        obj = Message.objects.create(
            message = 'Semangat pejuang corona',
            time = timezone.now()
        )

        self.assertTrue(isinstance(obj, Message))
        self.assertEqual(obj.__str__(), obj.message)

    def test_valid_form(self):
        obj = Message.objects.create(
            message = 'Semangat pejuang corona',
            time = timezone.now()
        )
        data = {
            'message' : obj.message,
            'time' : obj.time,
        }
        form = MessageForm(data=data)
        self.assertTrue(form.is_valid())

    def test_if_form_is_invalid(self):
        obj = Message.objects.create(
            message = '',
            time = timezone.now()
        )
        data = {
            'message' : obj.message,
            'time' : obj.time,
        }
        form = MessageForm(data=data)
        self.assertFalse(form.is_valid())