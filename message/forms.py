from .models import Message
from django import forms

class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = [
            'message',
        ]

        widgets = {
            'message' : forms.Textarea (
                attrs = {
                    'class' : 'form-control',
                    'placeholder' : 'Masukkan pesan kamu ...',
                    'rows' : '4',
                }
            ),
        }
