from django.shortcuts import render, redirect
from .models import Message
from .forms import MessageForm
from django.http import JsonResponse, HttpResponse

# Create your views here.
def view(request):
    message = Message.objects.all().order_by('-time')
    messageform = MessageForm(request.POST or None)
    if request.method == "POST":
        if messageform.is_valid():
            messageform.save()

            return redirect('/messagetext/')

    context = {
        'pesan' : message,
        'formmessage' :messageform,
    }
    return render(request, 'message/messagetext.html',context)

def ajax_data(request):

    data = list(Message.objects.values().order_by('-time'))
    return JsonResponse(data, safe=False)